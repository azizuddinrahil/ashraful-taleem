import { fetchBook } from "./service.js";
import { imageWidth } from "./app.js";
const book = {};

const pageImageElement = document.getElementById('page');

const render = summary => {
  page.src = summary.cover_page;
  page.width = imageWidth;
};

book.load = () => {
  fetchBook()
    .then(render)
}

export default book;
