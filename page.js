import { fetchPage } from "./service.js";
import { play } from "./audio_player.js";
import { imageWidth as imageWidthInApp } from "./app.js";

let rendered_pages = 0;
const pages = [];


const pageElement = document.getElementById('page');
const hotspotsContainer = document.getElementById('hotspots_container');

const getHotSpotSizeAndPosition = ( hotspotInfo, imageInfo ) => {
  const widthFactor = imageWidthInApp / imageInfo.width ;
  const imageHeightInApp = widthFactor * imageInfo.height;
  const heightFactor = imageHeightInApp / imageInfo.height;

  const factoredHotspotWidth = widthFactor * hotspotInfo.width;
  const factoredHotspotHeight = heightFactor * hotspotInfo.height;
  const factoredHotspotLeft = widthFactor * hotspotInfo.startPoint.x;
  const factoredHotspotTop = (imageHeightInApp / imageInfo.height) * hotspotInfo.startPoint.y;

  return {
    x: factoredHotspotLeft,
    y: factoredHotspotTop,
    width: factoredHotspotWidth,
    height: factoredHotspotHeight
  }
};

const setDimenstionsAndPosition = ( factoredHotspotInfo, hotspot ) => {
  hotspot.style.width = factoredHotspotInfo.width + 'px';
  hotspot.style.height = factoredHotspotInfo.height + 'px';
  hotspot.style.top = factoredHotspotInfo.y + 'px';
  hotspot.style.left = factoredHotspotInfo.x + 'px';
  console.log(hotspot.style);
};

const createHotspot = ( hotSpotInfo, imageInfo ) => {
  const hotspot = document.createElement('div');
  const factoredHotspotInfo = getHotSpotSizeAndPosition(hotSpotInfo, imageInfo);
  setDimenstionsAndPosition(factoredHotspotInfo, hotspot);
  hotspot.className = "hotspot";

  hotspot.addEventListener('click', () => play(hotSpotInfo.audioSrc));
  return hotspot;
};

const renderPage = pageInfo => {
  pageElement.src = pageInfo.image.src;
  pages.concat(pageInfo);
  pageInfo.hotSpots.map(hotspot => createHotspot(hotspot, pageInfo.image))
    .forEach(hotSpotElement => hotspotsContainer.appendChild(hotSpotElement))
};

export const renderNextPage = () => {
  fetchPage(++rendered_pages)
    .then(renderPage);
}

