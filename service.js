const baseUrl = ".";

const fetchJSON = (...args) => {
  return fetch(...args)
    .then(resp => {
      if(resp.ok) return resp.json();
      throw resp;
    })
}

export const fetchBook = () => fetchJSON(baseUrl + "/summary.json");
export const fetchPage = pageNumber => fetchJSON(baseUrl + `/pages/${pageNumber}.json`);
