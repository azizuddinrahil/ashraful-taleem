import book from "./book.js";
import { renderNextPage } from './page.js';

book.load();

const nextButton = document.getElementById('left_navigation_button');
nextButton.addEventListener('click', renderNextPage);

export const imageWidth = 500;
