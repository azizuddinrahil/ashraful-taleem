
const audioPlayerElement = document.getElementById('audio_player');

export const play = src => {
  audioPlayerElement.src=src;
  audioPlayerElement.play();
};
